
<!-- README.md is generated from README.Rmd. Please edit that file -->

# scribblr <img src='man/figures/logo.png' align="right" height="139" />

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
[![CRAN
status](https://www.r-pkg.org/badges/version/scribblr)](https://CRAN.R-project.org/package=scribblr)
[![R-CMD-check](https://github.com/vgherard/scribblr/workflows/R-CMD-check/badge.svg)](https://github.com/vgherard/scribblr/actions)
[![Codecov test
coverage](https://codecov.io/gh/vgherard/scribblr/branch/master/graph/badge.svg)](https://codecov.io/gh/vgherard/scribblr?branch=master)
[![Tweet](https://img.shields.io/twitter/url/http/shields.io.svg?style=social)](https://twitter.com/intent/tweet?text=%7Bscribblr%7D:%20A%20Minimalist%20Notepad%20Inside%20RStudio&url=https://github.com/vgherard/scribblr&via=ValerioGherardi&hashtags=rstats,rstudio,productivity)
<!-- badges: end -->

`scribblr` is a project-aware notepad inside RStudio.

![scribblr
demonstration](https://raw.githubusercontent.com/vgherard/scribblr/master/img/scribblr.gif)

## Installation

`scribblr` is not yet available on CRAN. In the meanwhile, you can
install the development version from [my R-universe
repository](https://vgherard.r-universe.dev/), with:

``` r
install.packages("scribblr", repos = "https://vgherard.r-universe.dev")
```

or equivalently from [GitHub](https://github.com/vgherard/scribblr)
with:

``` r
# install.packages("devtools")
devtools::install_github("vgherard/scribblr")
```
